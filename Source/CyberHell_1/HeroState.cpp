// Fill out your copyright notice in the Description page of Project Settings.


#include "HeroState.h"
#include "Animation/AnimInstance.h"
#include "Base_Weapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"
#include "Engine/Engine.h"
#include "Engine/World.h"
#include "Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "EnemyCharacter.h"
#include "EventSystem.h"
#include "CyberHellGameInstance.h"
#include "LevelBaseScriptActor.h"


FHeroState::FHeroState()
{
}

FHeroState::~FHeroState()
{
}

float FHeroState::AngleBetweenVectors(FVector FirstVector, FVector SecondVector)
{
	float Product = FVector::DotProduct(FirstVector, SecondVector);
	float FirstMod = FirstVector.Size();
	float SecondMod = SecondVector.Size();
	float Cosine = Product / (FirstMod * SecondMod);
	float Angle = FMath::RadiansToDegrees((FMath::Acos(Cosine)));

	return Angle;
}

bool FHeroState::CheckCanMoveLeft(ACyberHell_1Character& Character)
{
	FHitResult Hit = FHitResult();
	if (!Character.CanMoveLeftInLedgeArrow) { return false; }

	FVector Start = Character.CanMoveLeftInLedgeArrow->GetComponentLocation();
	FVector End = Start + Character.CanMoveLeftInLedgeArrow->GetForwardVector() * 50;
	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());
	FCollisionQueryParams TraceParams(FName(TEXT("ObstacleDetection Trace")), true, &Character);
	TraceParams.bTraceComplex = true;
	TraceParams.bIgnoreTouches = true;
	TraceParams.bReturnPhysicalMaterial = false;

	bool isHitReturned = Character.GetWorld()->SweepSingleByChannel(
		Hit,
		Start,
		End,
		Character.CanMoveLeftInLedgeArrow->GetComponentQuat().Identity,
		CollisionChannel,
		FCollisionShape::MakeCapsule(20.f, 60.f),
		TraceParams
	);

	return isHitReturned;
}

bool FHeroState::CheckCanMoveRight(ACyberHell_1Character& Character)
{
	FHitResult Hit = FHitResult();
	if (!Character.CanMoveRightInLedgeArrow) { return false; }

	FVector Start = Character.CanMoveRightInLedgeArrow->GetComponentLocation();
	FVector End = Start + Character.CanMoveRightInLedgeArrow->GetForwardVector() * 50;
	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());
	FCollisionQueryParams TraceParams(FName(TEXT("ObstacleDetection Trace")), true, &Character);
	TraceParams.bTraceComplex = true;
	TraceParams.bIgnoreTouches = true;
	TraceParams.bReturnPhysicalMaterial = false;

	bool isHitReturned = Character.GetWorld()->SweepSingleByChannel(
		Hit,
		Start,
		End,
		Character.CanMoveRightInLedgeArrow->GetComponentQuat().Identity,
		CollisionChannel,
		FCollisionShape::MakeCapsule(20.f, 60.f),
		TraceParams
	);

	return isHitReturned;
}

bool FHeroState::GetForwardVector(ACyberHell_1Character& Character, FVector& Location, FVector& Normal)
{
	FHitResult Hit = FHitResult();
	if (!Character.WallCheckArrow) { return false; }

	FVector Start = Character.WallCheckArrow->GetComponentLocation();
	FVector End = Start + (Character.WallCheckArrow->GetForwardVector() * Character.LenghtOfForwardTrace);
	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());

	bool IsHitReturned = Character.GetWorld()->SweepSingleByChannel(
		Hit,
		Start,
		End,
		FQuat(),
		CollisionChannel,
		FCollisionShape::MakeSphere(1),
		FCollisionQueryParams::FCollisionQueryParams(FName("Obstacle Tracer"), true, &Character)
	);
	
	if (IsHitReturned)
	{
		DrawDebugBox(Character.GetWorld(), Hit.Location, FVector(15, 15, 15), FColor(255, 0, 0), false, -1.0f, 0, 0.5f);
		Location = Hit.Location;
		Normal = Hit.Normal;

		return true;
	}

	return false;
}

bool FHeroState::GetUpwardVector(ACyberHell_1Character& Character, FVector& Height)
{
	FHitResult Hit = FHitResult();
	USkeletalMeshComponent* CharacterMesh = Character.GetMesh();
	FVector Start = Character.GetActorLocation();
	Start.Z += 200.f;
	Start += Character.GetActorForwardVector() * Character.LenghtOfUpwardTrace;
	FVector End = Start;
	End.Z -= 200.f;
	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());

	bool IsHitReturned = Character.GetWorld()->SweepSingleByChannel(
		Hit,
		Start,
		End,
		FQuat(),
		CollisionChannel,
		FCollisionShape::MakeBox(FVector(4, 4, 4))
	);
	
	FVector ClimbingCheckVector = FVector(0.f, 0.f, 0.f);

	if (CharacterMesh && IsHitReturned)
	{
		DrawDebugBox(Character.GetWorld(), Hit.Location, FVector(4, 4, 4), FColor(255, 0, 0), false, -1.0f, 0, 0.5f);
		Height = Hit.Location;
		ClimbingCheckVector.Z = WallHeight.Z - CharacterMesh->GetSocketLocation(Character.GetPelvisSocket()).Z;

		if (ClimbingCheckVector.Z > Character.GetMinClimbHeight() && ClimbingCheckVector.Z < Character.GetMaxClimbHeight())
		{
			return true;
		}
	}

	return false;
}

bool FHeroState::CheckCanHang(ACyberHell_1Character& Character, FVector& Location, FVector& Normal, FVector& Height)
{
	if (GetForwardVector(Character, Location, Normal) && GetUpwardVector(Character, Height))
	{
		return true;
	}

	return false;
}

bool FHeroState::CheckCanTurnLeft(ACyberHell_1Character& Character)
{
	FHitResult Hit = FHitResult();
	FHitResult HitWall = FHitResult();
	if (!Character.LeftJumpFromLedgeCheckArrow) { return true; }
	if (!Character.CanMoveLeftInLedgeArrow) { return true; }

	FVector Start = Character.LeftJumpFromLedgeCheckArrow->GetComponentLocation();
	Start.Z += 60.f;
	FVector End = Start + Character.LeftJumpFromLedgeCheckArrow->GetForwardVector() * 70;

	FVector WallCheckStartTrace = Character.CanMoveLeftInLedgeArrow->GetComponentLocation();
	FVector WallCheckEndTrace = WallCheckStartTrace + Character.CanMoveLeftInLedgeArrow->GetForwardVector() * 150;

	ECollisionChannel CollisionChannelForWall = UEngineTypes::ConvertToCollisionChannel(Character.GetStaticWallChannel());
	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());

	bool isHitReturned = Character.GetWorld()->SweepSingleByChannel(
		Hit,
		Start,
		End,
		Character.LeftJumpFromLedgeCheckArrow->GetComponentQuat().Identity,
		CollisionChannel,
		FCollisionShape::MakeSphere(20.f)
	);

	bool IsStaticWallHitReturned = Character.GetWorld()->SweepSingleByObjectType(
		HitWall,
		WallCheckStartTrace,
		WallCheckEndTrace,
		FQuat(),
		CollisionChannelForWall,
		FCollisionShape::MakeSphere(5.f)
	);

	if (isHitReturned || IsStaticWallHitReturned)
	{
		return false;
	}

	return true;
}

bool FHeroState::CheckCanTurnRight(ACyberHell_1Character& Character)
{
	FHitResult Hit = FHitResult();
	FHitResult HitWall = FHitResult();
	if (!Character.RightJumpFromLedgeCheckArrow) { return true; }
	if (!Character.CanMoveRightInLedgeArrow) { return true; }

	FVector Start = Character.RightJumpFromLedgeCheckArrow->GetComponentLocation();
	Start.Z += 60.f;
	FVector End = Start + Character.RightJumpFromLedgeCheckArrow->GetForwardVector() * 70;

	FVector WallCheckStartTrace = Character.CanMoveRightInLedgeArrow->GetComponentLocation();
	FVector WallCheckEndTrace = WallCheckStartTrace + Character.CanMoveRightInLedgeArrow->GetForwardVector() * 150;

	ECollisionChannel CollisionChannelForWall = UEngineTypes::ConvertToCollisionChannel(Character.GetStaticWallChannel());
	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());

	bool isHitReturned = Character.GetWorld()->SweepSingleByChannel(
		Hit,
		Start,
		End,
		Character.RightJumpFromLedgeCheckArrow->GetComponentQuat().Identity,
		CollisionChannel,
		FCollisionShape::MakeSphere(20.f)
	);

	bool IsStaticWallHitReturned = Character.GetWorld()->SweepSingleByObjectType(
		HitWall,
		WallCheckStartTrace,
		WallCheckEndTrace,
		FQuat(),
		CollisionChannelForWall,
		FCollisionShape::MakeSphere(5.f)
	);

	if (isHitReturned || IsStaticWallHitReturned)
	{
		return false;
	}

	return true;
}

bool FHeroState::CheckCanJumpLeft(ACyberHell_1Character& Character)
{
	FHitResult Hit = FHitResult();
	if (!Character.LeftJumpFromLedgeCheckArrow) { return false; }

	FVector Start = Character.LeftJumpFromLedgeCheckArrow->GetComponentLocation();
	FVector End = Start + Character.LeftJumpFromLedgeCheckArrow->GetForwardVector() * 50;

	ECollisionChannel CollisionChannelForJump = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());

	bool IsHitReturned = Character.GetWorld()->SweepSingleByChannel(
		Hit,
		Start,
		End,
		Character.LeftJumpFromLedgeCheckArrow->GetComponentQuat().Identity,
		CollisionChannelForJump,
		FCollisionShape::MakeCapsule(25.f, 60.f)
	);


	if (IsHitReturned)
	{
		return true;
	}

	return false;
}

bool FHeroState::CheckCanJumpRight(ACyberHell_1Character& Character)
{
	FHitResult Hit = FHitResult();
	if (!Character.RightJumpFromLedgeCheckArrow) { return false; }

	FVector Start = Character.RightJumpFromLedgeCheckArrow->GetComponentLocation();
	FVector End = Start + Character.RightJumpFromLedgeCheckArrow->GetForwardVector() * 50;
	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());

	bool isHitReturned = Character.GetWorld()->SweepSingleByChannel(
		Hit,
		Start,
		End,
		Character.RightJumpFromLedgeCheckArrow->GetComponentQuat().Identity,
		CollisionChannel,
		FCollisionShape::MakeCapsule(25.f, 60.f)
	);

	if (isHitReturned)
	{
		return true;
	}

	return false;
}

bool FHeroState::CheckLeftWall(ACyberHell_1Character& Character)
{
	FHitResult Hit = FHitResult();
	if (!Character.LeftWallCheckArrow) { return true; }

	FVector Start = Character.LeftWallCheckArrow->GetComponentLocation();
	FVector End = Start + Character.LeftWallCheckArrow->GetForwardVector() * 100;
	FCollisionQueryParams TraceParams(FName(TEXT("ObstacleDetection Trace")), true, &Character);
	TraceParams.bTraceComplex = true;
	TraceParams.bIgnoreTouches = true;
	TraceParams.bReturnPhysicalMaterial = false;
	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());

	bool isHitReturned = Character.GetWorld()->LineTraceSingleByChannel(
		Hit,
		Start,
		End,
		CollisionChannel,
		TraceParams
	);

	if (isHitReturned)
	{
		DrawDebugLine(Character.GetWorld(), Start, End, FColor(255, 0, 255), false, -1.f, 0, 1.f);
		return false;
	}

	return true;
}

bool FHeroState::CheckRightWall(ACyberHell_1Character& Character)
{
	FHitResult Hit = FHitResult();
	if (!Character.RightWallCheckArrow) { return true; }

	FVector Start = Character.RightWallCheckArrow->GetComponentLocation();
	FVector End = Start + Character.RightWallCheckArrow->GetForwardVector() * 100;
	FCollisionQueryParams TraceParams(FName(TEXT("ObstacleDetection Trace")), true, &Character);
	TraceParams.bTraceComplex = true;
	TraceParams.bIgnoreTouches = true;
	TraceParams.bReturnPhysicalMaterial = false;
	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());

	bool isHitReturned = Character.GetWorld()->LineTraceSingleByChannel(
		Hit,
		Start,
		End,
		CollisionChannel,
		TraceParams
	);

	if (isHitReturned)
	{
		DrawDebugLine(Character.GetWorld(), Start, End, FColor(255, 0, 255), false, -1.f, 0, 1.f);
		return false;
	}

	return true;
}

void FHeroState::EquipWeapon(ACyberHell_1Character& Character)
{
	FAttachmentTransformRules TransformRules = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true);
	if (Character.GetEquippedWeapon() != nullptr)
	{
		Character.GetEquippedWeapon()->AttachToComponent(Character.GetMesh(),
			TransformRules,
			Character.GetEquippedWeapon()->EquipSocket);
	}
}

void FHeroState::UnequipWeapon(ACyberHell_1Character& Character)
{
	FAttachmentTransformRules TransformRules = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true);
	if (Character.GetEquippedWeapon() != nullptr)
	{
		Character.GetEquippedWeapon()->AttachToComponent(Character.GetMesh(),
			TransformRules,
			Character.GetEquippedWeapon()->UnequipSocket);
	}
}

bool FHeroState::FindEnemyToLockOn(ACyberHell_1Character& Character, TArray<AActor*> Enemies)
{
	if (Enemies.Num() > 0)
	{
		Enemies.Sort([&](const AActor& t1, const AActor& t2) {
			return FVector::Distance(t1.GetActorLocation(), Character.GetActorLocation()) <
				FVector::Distance(t2.GetActorLocation(), Character.GetActorLocation()); });
		
		for (AActor* Enemy : Enemies)
		{
			if (Character.GetDistanceTo(Enemy) < Character.GetRangeToLockOn())
			{
				FHitResult Hit;
				ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetStaticWallChannel());

				bool IsHitReturned = Character.GetWorld()->LineTraceSingleByObjectType(
					Hit,
					Character.GetActorLocation(),
					Enemy->GetActorLocation(),
					CollisionChannel
				);

				if (!IsHitReturned)
				{
					Character.SetCurrentLockedOnEnemy(Enemy);

					UE_LOG(LogTemp, Warning, TEXT("%s"), *Character.GetCurrentLockedOnEnemy()->GetActorLabel());

					if (Character.GetCurrentLockedOnEnemy() != nullptr)
					{
						Character.bIsLerping = true;
						Character.GameInstance->EventHandler->OnEnemyLockOn.Broadcast(Character.GetCurrentLockedOnEnemy()->GetUniqueID());

						return true;
					}
				}
				else
				{
					UE_LOG(LogTemp, Warning, TEXT("There is a wall"));

					continue;
				}
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Too far away"));

				if (Character.GetCurrentLockedOnEnemy() != nullptr)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}
	else
	{
		if (Character.GetCurrentLockedOnEnemy() != nullptr)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

bool FHeroState::ChangeTargetLockOn(ACyberHell_1Character& Character, float MouseInput)
{
	TArray<AActor*> AllEnemies;
	ALevelBaseScriptActor* LevelActor = Cast<ALevelBaseScriptActor>(Character.GetLevel()->GetLevelScriptActor());
	
	for (AActor* Enemy : LevelActor->EnemyStorage)
	{
		FVector NewEnemyDirection = (Enemy->GetActorLocation() - Character.GetActorLocation()).GetSafeNormal();
		FVector PlayerForwardVector = Character.GetActorForwardVector();

		FVector VectorMultiplication = FVector::CrossProduct(PlayerForwardVector, NewEnemyDirection);

		if (MouseInput * VectorMultiplication.Z > 0.02f)
		{
			AllEnemies.Add(Enemy);
		}
	}

	FindEnemyToLockOn(Character, AllEnemies);

	return false;
}

/////////////////////////////////
///RUN_STATE_SECTION////////////
///////////////////////////////

void FHeroModeRun::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	if (CurrentCooldownHangingStateTime < CooldownHangingState)
	{
		CurrentCooldownHangingStateTime += DeltaTime;
	}
}

FHeroState* FHeroModeRun::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (PlayerController == nullptr)
	{
		return nullptr;
	}

	if (CheckCorners(Character) && CurrentCooldownHangingStateTime >= CooldownHangingState)
	{
		if (CheckCanHang(Character, WallLocation, WallNormal, WallHeight))
		{
			return new FHeroModeHang();
		}
	}

	if (Character.GetCanEquipWeapon() && PlayerController->WasInputKeyJustPressed(EKeys::E))
	{
		if (Character.GetEquippedWeapon() == nullptr && Character.AttachedWeapon != nullptr)
		{
			AttachWeaponToPlayer(Character);
			return new FHeroModeRunWithWeapon();
		}
	}

	if (Character.GetEquippedWeapon() != nullptr && PlayerController->WasInputKeyJustPressed(EKeys::Tab))
	{
		return new FHeroModeDrawingWeapon();
	}

	return nullptr;
}

void FHeroModeRun::OnEnterState(ACyberHell_1Character& Character)
{
	CurrentCooldownHangingStateTime = 0.f;
	Character.GetCharacterMovement()->SetMovementMode(MOVE_Walking);
	Character.GetCharacterMovement()->MaxWalkSpeed = 600;
}

void FHeroModeRun::OnExitState(ACyberHell_1Character& Character)
{
	
}

bool FHeroModeRun::CheckCorners(ACyberHell_1Character& Character)
{
	FHitResult LeftHit = FHitResult();
	FHitResult RightHit = FHitResult();
	FVector LeftStart = Character.LeftCornerCheckArrow->GetComponentLocation();
	FVector LeftEnd = LeftStart + Character.LeftCornerCheckArrow->GetForwardVector() * Character.LenghtOfForwardTrace;
	FVector RightStart = Character.RightCornerCheckArrow->GetComponentLocation();
	FVector RightEnd = RightStart + Character.RightCornerCheckArrow->GetForwardVector() * Character.LenghtOfForwardTrace;
	FCollisionQueryParams TraceParams(FName(TEXT("ObstacleDetection Trace")), true, &Character);
	TraceParams.bTraceComplex = true;
	TraceParams.bIgnoreTouches = true;
	TraceParams.bReturnPhysicalMaterial = false;
	TraceParams.AddIgnoredActor(&Character);
	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());

	bool isLeftHitReturned = Character.GetWorld()->LineTraceSingleByChannel(
		LeftHit,
		LeftStart,
		LeftEnd,
		CollisionChannel,
		TraceParams
	);

	bool isRightHitReturned = Character.GetWorld()->LineTraceSingleByChannel(
		RightHit,
		RightStart,
		RightEnd,
		CollisionChannel,
		TraceParams
	);

	if (isLeftHitReturned && isRightHitReturned)
	{
		FVector LeftNormal = LeftHit.Location + LeftHit.Normal * 100;
		FVector RightNormal = RightHit.Location + RightHit.Normal * 100;

		FVector FromPlayerLeft = (LeftEnd - LeftStart).GetSafeNormal();
		FVector FromPlayerRight = (RightEnd - RightStart).GetSafeNormal();

		float LeftAngle = AngleBetweenVectors(FromPlayerLeft, LeftHit.Normal);
		float RightAngle = AngleBetweenVectors(FromPlayerRight, RightHit.Normal);

		if (LeftAngle == RightAngle)
		{
			return true;
		}
	}
	return false;
}

void FHeroModeRun::AttachWeaponToPlayer(ACyberHell_1Character& Character)
{
	Character.SetRunWithWeapon(true);
	FTransform SpawnTransform = Character.GetMesh()->GetSocketTransform(Character.AttachedWeapon->EquipSocket);
	ABase_Weapon* Weapon = Character.GetWorld()->SpawnActor<ABase_Weapon>(Character.AttachedWeapon->GetClass(), SpawnTransform);
	Character.SetEquippedWeapon(Weapon);
	EquipWeapon(Character);
}

/////////////////////////////////
///HANG_STATE_SECTION///////////
///////////////////////////////

void FHeroModeHang::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	
}

FHeroState* FHeroModeHang::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (PlayerController == nullptr)
	{
		return nullptr;
	}

	bool bCheckMoveLeftInput = CheckMoveLeftInput(Character);
	bool bCheckMoveRightInput = CheckMoveRightInput(Character);

	if (PlayerController->WasInputKeyJustPressed(EKeys::C))
	{
		return new FHeroModeRun();
	}

	if (!bCheckMoveLeftInput && !bCheckMoveRightInput && PlayerController->WasInputKeyJustPressed(EKeys::SpaceBar))
	{
		if (!CheckStaticWall(Character))
		{
			return new FHeroModeClimbing();
		}
	}

	if (bCheckMoveLeftInput)
	{
		if (CheckCanMoveLeft(Character) && CheckLeftWall(Character))
		{
			return new FHeroModeMoveLeftInLedge();
		}
		else if (CheckCanTurnLeft(Character) && !CheckCanJumpLeft(Character))
		{
			return new FHeroModeTurnLeftInLedge();
		}
		else if (CheckCanJumpLeft(Character) && PlayerController->WasInputKeyJustPressed(EKeys::SpaceBar))
		{
			return new FHeroModeJumpLeftFromLedge();
		}
	}

	if (bCheckMoveRightInput)
	{
		if (CheckCanMoveRight(Character) && CheckRightWall(Character))
		{
			return new FHeroModeMoveRightInLedge();
		}
		else if (CheckCanTurnRight(Character) && !CheckCanJumpRight(Character))
		{
			return new FHeroModeTurnRightInLedge();
		}
		else if (CheckCanJumpRight(Character) && PlayerController->WasInputKeyJustPressed(EKeys::SpaceBar))
		{
			return new FHeroModeJumpRightFromLedge();
		}
	}

	if (CheckTurnBackInput(Character))
	{
		return new FHeroModeTurnBackInLedge();
	}

	return nullptr;
}

void FHeroModeHang::OnEnterState(ACyberHell_1Character& Character)
{
	CheckCanHang(Character, WallLocation, WallNormal, WallHeight);
	Character.GetCharacterMovement()->SetMovementMode(MOVE_Flying);
	Character.SetHangingIdle(true);

	float XLocation = WallNormal.X * Character.XHangingOffset + WallLocation.X;
	float YLocation = WallNormal.Y * Character.YHangingOffset + WallLocation.Y;
	float ZLocation = WallHeight.Z - Character.ZHangingOffset;
	FLatentActionInfo Info;
	Info.CallbackTarget = &Character;
	Info.ExecutionFunction = FName("StopMovement");
	Info.UUID = 1;
	Info.Linkage = 0;

	
	UKismetSystemLibrary::MoveComponentTo(
		Character.GetCapsuleComponent(),
		FVector(XLocation, YLocation, ZLocation),
		FRotator(0, WallNormal.Rotation().Yaw - 180.f, WallNormal.Rotation().Roll),
		false,
		false,
		0.01f,
		false,
		EMoveComponentAction::Move,
		Info
	);
}

void FHeroModeHang::OnExitState(ACyberHell_1Character& Character)
{
	Character.SetHangingIdle(false);
}

bool FHeroModeHang::CheckMoveLeftInput(ACyberHell_1Character& Character)
{
	FVector CharacterLeftVector = -Character.GetActorRightVector();
	FVector CameraForwardVector = FVector(Character.GetCameraBoom()->GetForwardVector().X,
		Character.GetCameraBoom()->GetForwardVector().Y, 0.f);

	float Angle = AngleBetweenVectors(CharacterLeftVector, CameraForwardVector);

	if (Character.GetInputAxisValue("MoveRight") < 0 && Angle > 45.f && Angle < 135.f)
	{
		return true;
	}

	if (Character.GetInputAxisValue("MoveForward") > 0 && Angle < 45.f)
	{
		return true;
	}

	if (Character.GetInputAxisValue("MoveForward") < 0 && Angle > 135.f)
	{
		return true;
	}

	return false;
}

bool FHeroModeHang::CheckMoveRightInput(ACyberHell_1Character& Character)
{
	FVector CharacterRightVector = Character.GetActorRightVector();
	FVector CameraForwardVector = FVector(Character.GetCameraBoom()->GetForwardVector().X,
		Character.GetCameraBoom()->GetForwardVector().Y, 0.f);

	float Angle = AngleBetweenVectors(CharacterRightVector, CameraForwardVector);

	if (Character.GetInputAxisValue("MoveRight") > 0 && Angle > 45.f && Angle < 135.f)
	{
		return true;
	}

	if (Character.GetInputAxisValue("MoveForward") < 0 && Angle > 135.f)
	{
		return true;
	}

	if (Character.GetInputAxisValue("MoveForward") > 0 && Angle < 45.f)
	{
		return true;
	}

	return false;
}

bool FHeroModeHang::CheckTurnBackInput(ACyberHell_1Character& Character)
{
	FVector CharacterLeftVector = -Character.GetActorRightVector();
	FVector CameraForwardVector = FVector(Character.GetCameraBoom()->GetForwardVector().X,
		Character.GetCameraBoom()->GetForwardVector().Y, 0.f);

	float Angle = AngleBetweenVectors(CharacterLeftVector, CameraForwardVector);

	if (Character.GetInputAxisValue("MoveForward") < 0 && Angle > 45.f && Angle < 135.f)
	{
		return true;
	}

	if (Character.GetInputAxisValue("MoveRight") < 0 && Angle < 45.f)
	{
		return true;
	}

	if (Character.GetInputAxisValue("MoveRight") > 0 && Angle > 135.f)
	{
		return true;
	}

	return false;
}

bool FHeroModeHang::CheckDownLedge(ACyberHell_1Character& Character)
{
	FHitResult Hit;
	FVector Start = WallHeight - FVector(0, 0, 100.f);
	FVector End = WallHeight - FVector(0, 0, 500.f);

	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetTraceChannel());

	bool isHitReturned = Character.GetWorld()->SweepSingleByChannel(
		Hit,
		Start,
		End,
		FQuat(),
		CollisionChannel,
		FCollisionShape::MakeSphere(5.f)
	);

	if (isHitReturned)
	{
		return true;
	}

	return false;
}

bool FHeroModeHang::CheckStaticWall(ACyberHell_1Character& Character)
{
	FHitResult Hit;
	FVector Start = Character.StaticWallCheckArrow->GetComponentLocation();
	FVector End = Start + Character.StaticWallCheckArrow->GetForwardVector() * 100;

	ECollisionChannel CollisionChannel = UEngineTypes::ConvertToCollisionChannel(Character.GetStaticWallChannel());
	
	bool isHitReturned = Character.GetWorld()->SweepSingleByObjectType(
		Hit,
		Start,
		End,
		FQuat(),
		CollisionChannel,
		FCollisionShape::MakeSphere(5.f)
	);

	if (isHitReturned)
	{
		return true;
	}

	return false;
}

/////////////////////////////////
///CLIMBING_STATE_SECTION///////
///////////////////////////////

void FHeroModeClimbing::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	CurrentAnimationTime += DeltaTime;
}

FHeroState* FHeroModeClimbing::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (CurrentAnimationTime >= AnimationDuration)
	{
		return new FHeroModeRun();
	}

	return nullptr;
}

void FHeroModeClimbing::OnEnterState(ACyberHell_1Character& Character)
{
	CurrentAnimationTime = 0.f;
	Character.EnableMovement(false);

	if (Character.GetMesh()->GetAnimInstance() && Character.GetClimbMontage())
	{
		AnimationDuration = Character.GetMesh()->GetAnimInstance()->Montage_Play(Character.GetClimbMontage());
	}

	Character.GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void FHeroModeClimbing::OnExitState(ACyberHell_1Character& Character)
{
	Character.GetCharacterMovement()->SetMovementMode(MOVE_Walking);
	Character.EnableMovement(true);
	Character.GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

///////////////////////////////////////
///MOVE_LEFT_IN_LEDGE_STATE_SECTION///
/////////////////////////////////////

void FHeroModeMoveLeftInLedge::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	FVector Current = Character.GetActorLocation();
	FVector Target = Current - Character.GetActorQuat().GetRightVector() * 20.f;
	FVector NewLocation = FMath::VInterpTo(Current, Target, Character.GetWorld()->GetDeltaSeconds(), 5.f);
	Character.SetActorLocation(NewLocation);
}

FHeroState* FHeroModeMoveLeftInLedge::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (Character.GetInputAxisValue("MoveRight") == 0 && Character.GetInputAxisValue("MoveForward") == 0 ||
		!CheckCanMoveLeft(Character) || !CheckLeftWall(Character))
	{
		return new FHeroModeHang();
	}

	if (CheckCanJumpLeft(Character) && PlayerController->WasInputKeyJustPressed(EKeys::SpaceBar))
	{
		return new FHeroModeJumpLeftFromLedge();
	}

	return nullptr;
}

void FHeroModeMoveLeftInLedge::OnEnterState(ACyberHell_1Character& Character)
{
	Character.SetHangingIdle(true);
	Character.SetMovingLeftInLedge(true);
}

void FHeroModeMoveLeftInLedge::OnExitState(ACyberHell_1Character& Character)
{
	Character.SetHangingIdle(false);
	Character.SetMovingLeftInLedge(false);
}

///////////////////////////////////////
///MOVE_RIGHT_IN_LEDGE_STATE_SECTION//
/////////////////////////////////////

void FHeroModeMoveRightInLedge::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	FVector Current = Character.GetActorLocation();
	FVector Target = Current + Character.GetActorQuat().GetRightVector() * 20.f;
	FVector NewLocation = FMath::VInterpTo(Current, Target, Character.GetWorld()->GetDeltaSeconds(), 5.f);
	Character.SetActorLocation(NewLocation);
}

FHeroState* FHeroModeMoveRightInLedge::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (Character.GetInputAxisValue("MoveRight") == 0 && Character.GetInputAxisValue("MoveForward") == 0 ||
		!CheckCanMoveRight(Character) || !CheckRightWall(Character))
	{
		return new FHeroModeHang();
	}

	if (CheckCanJumpRight(Character) && PlayerController->WasInputKeyJustPressed(EKeys::SpaceBar))
	{
		return new FHeroModeJumpRightFromLedge();
	}

	return nullptr;
}

void FHeroModeMoveRightInLedge::OnEnterState(ACyberHell_1Character& Character)
{
	Character.SetHangingIdle(true);
	Character.SetMovingRightInLedge(true);
}

void FHeroModeMoveRightInLedge::OnExitState(ACyberHell_1Character& Character)
{
	Character.SetHangingIdle(false);
	Character.SetMovingRightInLedge(false);
}

///////////////////////////////////////
///JUMP_LEFT_FROM_LEDGE_STATE_SECTION/
/////////////////////////////////////

void FHeroModeJumpLeftFromLedge::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	CurrentAnimationTime += DeltaTime;
}

FHeroState* FHeroModeJumpLeftFromLedge::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (CurrentAnimationTime >= AnimationDuration)
	{
		return new FHeroModeHang();
	}

	return nullptr;
}

void FHeroModeJumpLeftFromLedge::OnEnterState(ACyberHell_1Character& Character)
{
	Character.SetTempStateForHanging(true);
	CurrentAnimationTime = 0.f;
	Character.DisableInput(Character.GetWorld()->GetFirstPlayerController());

	if (Character.GetMesh()->GetAnimInstance() && Character.GetJumpLeftFromLedgeMontage())
	{
		AnimationDuration = Character.GetMesh()->GetAnimInstance()->Montage_Play(Character.GetJumpLeftFromLedgeMontage());
	}
}

void FHeroModeJumpLeftFromLedge::OnExitState(ACyberHell_1Character& Character)
{
	Character.SetTempStateForHanging(false);
	Character.EnableInput(Character.GetWorld()->GetFirstPlayerController());
}

////////////////////////////////////////
///JUMP_RIGHT_FROM_LEDGE_STATE_SECTION/
//////////////////////////////////////

void FHeroModeJumpRightFromLedge::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	CurrentAnimationTime += DeltaTime;
}

FHeroState* FHeroModeJumpRightFromLedge::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (CurrentAnimationTime >= AnimationDuration)
	{
		return new FHeroModeHang();
	}

	return nullptr;
}

void FHeroModeJumpRightFromLedge::OnEnterState(ACyberHell_1Character& Character)
{
	Character.SetTempStateForHanging(true);
	CurrentAnimationTime = 0.f;
	Character.DisableInput(Character.GetWorld()->GetFirstPlayerController());

	if (Character.GetMesh()->GetAnimInstance() && Character.GetJumpRightFromLedgeMontage())
	{
		AnimationDuration = Character.GetMesh()->GetAnimInstance()->Montage_Play(Character.GetJumpRightFromLedgeMontage());
	}
}

void FHeroModeJumpRightFromLedge::OnExitState(ACyberHell_1Character& Character)
{
	Character.SetTempStateForHanging(false);
	Character.EnableInput(Character.GetWorld()->GetFirstPlayerController());
}

////////////////////////////////////////
///TURN_LEFT_IN_LEDGE_STATE_SECTION////
//////////////////////////////////////

void FHeroModeTurnLeftInLedge::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	
}

FHeroState* FHeroModeTurnLeftInLedge::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (bIsMontageEnded)
	{
		return new FHeroModeHang();
	}

	return nullptr;
}

void FHeroModeTurnLeftInLedge::OnEnterState(ACyberHell_1Character& Character)
{
	bIsMontageEnded = false;
	Character.SetHangingIdle(true);
	Character.SetTempStateForHanging(true);
	Character.DisableInput(Character.GetWorld()->GetFirstPlayerController());

	if (Character.GetMesh()->GetAnimInstance() && Character.GetTurnLeftInLedgeMontage())
	{
		Character.GetMesh()->GetAnimInstance()->Montage_Play(Character.GetTurnLeftInLedgeMontage());
	}

	Delegate.BindRaw(this, &FHeroModeTurnLeftInLedge::IsMontageEnded);
	Character.GetMesh()->GetAnimInstance()->Montage_SetBlendingOutDelegate(Delegate);
}

void FHeroModeTurnLeftInLedge::OnExitState(ACyberHell_1Character& Character)
{
	//Character.SetHangingIdle(false);
	Character.SetTempStateForHanging(false);
	Character.EnableInput(Character.GetWorld()->GetFirstPlayerController());
}

void FHeroModeTurnLeftInLedge::IsMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	bIsMontageEnded = true;
}

////////////////////////////////////////
///TURN_RIGHT_IN_LEDGE_STATE_SECTION///
//////////////////////////////////////

void FHeroModeTurnRightInLedge::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	
}

FHeroState* FHeroModeTurnRightInLedge::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (bIsMontageEnded)
	{
		return new FHeroModeHang();
	}

	return nullptr;
}

void FHeroModeTurnRightInLedge::OnEnterState(ACyberHell_1Character& Character)
{
	bIsMontageEnded = false;
	Character.SetHangingIdle(true);
	Character.SetTempStateForHanging(true);
	Character.DisableInput(Character.GetWorld()->GetFirstPlayerController());

	if (Character.GetMesh()->GetAnimInstance() && Character.GetTurnRightInLedgeMontage())
	{
		Character.GetMesh()->GetAnimInstance()->Montage_Play(Character.GetTurnRightInLedgeMontage());
	}

	Delegate.BindRaw(this, &FHeroModeTurnRightInLedge::IsMontageEnded);
	Character.GetMesh()->GetAnimInstance()->Montage_SetBlendingOutDelegate(Delegate);
}

void FHeroModeTurnRightInLedge::OnExitState(ACyberHell_1Character& Character)
{
	//Character.SetHangingIdle(false);
	Character.SetTempStateForHanging(false);
	Character.EnableInput(Character.GetWorld()->GetFirstPlayerController());
}

void FHeroModeTurnRightInLedge::IsMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	bIsMontageEnded = true;
}

////////////////////////////////////////
///TURN_BACK_IN_LEDGE_STATE_SECTION////
//////////////////////////////////////

void FHeroModeTurnBackInLedge::Tick(ACyberHell_1Character& Character, float DeltaTime)
{

}

FHeroState* FHeroModeTurnBackInLedge::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (Character.GetInputAxisValue("MoveForward") == 0 && Character.GetInputAxisValue("MoveRight") == 0)
	{
		return new FHeroModeHang();
	}

	if (PlayerController->WasInputKeyJustPressed(EKeys::SpaceBar))
	{
		if (Character.GetJumpFromWallMontage() != nullptr)
		{
			return new FHeroModeJumpFromWall();
		}
		else
		{
			JumpFromLedge(Character);
			return new FHeroModeRun();
		}
	}

	return nullptr;
}

void FHeroModeTurnBackInLedge::OnEnterState(ACyberHell_1Character& Character)
{
	Character.SetTurnBackInLedge(true);
	Character.SetHangingIdle(true);
}

void FHeroModeTurnBackInLedge::OnExitState(ACyberHell_1Character& Character)
{
	Character.SetTurnBackInLedge(false);
	Character.SetHangingIdle(false);
}

void FHeroModeTurnBackInLedge::JumpFromLedge(ACyberHell_1Character& Character)
{
	FVector VectorVelocity = Character.GetActorForwardVector() * (Character.ForwardJumpPower) + FVector(0, 0, Character.UpwardJumpPower);
	Character.LaunchCharacter(VectorVelocity, false, false);
	FRotator CharacterRotation = Character.GetActorRotation();
	Character.SetActorRotation(FRotator(CharacterRotation.Pitch, CharacterRotation.Yaw + 120, CharacterRotation.Roll));
}

////////////////////////////////////////
///RUN_WITH_WEAPON_STATE_SECTION///////
//////////////////////////////////////

void FHeroModeRunWithWeapon::Tick(ACyberHell_1Character& Character, float DeltaTime)
{

}

FHeroState* FHeroModeRunWithWeapon::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (PlayerController->WasInputKeyJustPressed(EKeys::Tab))
	{
		return new FHeroModeSheathingWeapon();
	}

	if (PlayerController->WasInputKeyJustPressed(EKeys::LeftMouseButton))
	{
		return new FHeroModeLightCombo(0);
	}

	/*if (PlayerController->WasInputKeyJustPressed(EKeys::RightMouseButton))
	{
		return new FHeroModeHeavyCombo(0);
	}*/

	if (PlayerController->WasInputKeyJustPressed(EKeys::F))
	{
		Character.SetIsCharacterLockedOn(FindEnemyToLockOn(Character, LevelActor->EnemyStorage));

		if (Character.GetCurrentLockedOnEnemy() != nullptr)
		{
			return new FHeroModeLockedOn();
		}
	}

	return nullptr;
}

void FHeroModeRunWithWeapon::OnEnterState(ACyberHell_1Character& Character)
{
	Character.GetCharacterMovement()->SetMovementMode(MOVE_Walking);
	Character.EnableCameraRotationByPlayer(true);
	LevelActor = Cast<ALevelBaseScriptActor>(Character.GetLevel()->GetLevelScriptActor());
}

void FHeroModeRunWithWeapon::OnExitState(ACyberHell_1Character& Character)
{

}

////////////////////////////////////////
///SHEATH_WEAPON_STATE_SECTION/////////
//////////////////////////////////////

void FHeroModeSheathingWeapon::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	CurrentAnimationTime += DeltaTime;
}

FHeroState* FHeroModeSheathingWeapon::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (CurrentAnimationTime >= AnimationDuration)
	{
		return new FHeroModeRun();
	}

	return nullptr;
}

void FHeroModeSheathingWeapon::OnEnterState(ACyberHell_1Character& Character)
{
	Character.SetRunWithWeapon(false);
	CurrentAnimationTime = 0.f;
	Character.EnableMovement(false);

	if (Character.GetMesh()->GetAnimInstance() && Character.GetEquippedWeapon()->SheathWeaponMontage)
	{
		AnimationDuration = Character.GetMesh()->GetAnimInstance()->Montage_Play(Character.GetEquippedWeapon()->SheathWeaponMontage);
	}
}

void FHeroModeSheathingWeapon::OnExitState(ACyberHell_1Character& Character)
{
	UnequipWeapon(Character);
	Character.EnableMovement(true);
}

////////////////////////////////////////
///DRAW_WEAPON_STATE_SECTION///////////
//////////////////////////////////////

void FHeroModeDrawingWeapon::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	CurrentAnimationTime += DeltaTime;
}

FHeroState* FHeroModeDrawingWeapon::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (CurrentAnimationTime >= AnimationDuration)
	{
		return new FHeroModeRunWithWeapon();
	}

	return nullptr;
}

void FHeroModeDrawingWeapon::OnEnterState(ACyberHell_1Character& Character)
{
	Character.SetRunWithWeapon(true);
	EquipWeapon(Character);
	CurrentAnimationTime = 0.f;
	Character.EnableMovement(false);

	if (Character.GetMesh()->GetAnimInstance() && Character.GetEquippedWeapon()->DrawWeaponMontage)
	{
		AnimationDuration = Character.GetMesh()->GetAnimInstance()->Montage_Play(Character.GetEquippedWeapon()->DrawWeaponMontage);
	}
}

void FHeroModeDrawingWeapon::OnExitState(ACyberHell_1Character& Character)
{
	Character.EnableMovement(true);
}

////////////////////////////////////////
///LIGHT_COMBO_STATE_SECTION///////////
//////////////////////////////////////

void FHeroModeLightCombo::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	CurrentAnimationTime += DeltaTime;

	if (CurrentAnimationTime >= Character.GetEquippedWeapon()->LightCombo[Index].StartApplyDamageTime &&
		CurrentAnimationTime <= Character.GetEquippedWeapon()->LightCombo[Index].EndApplyDamageTime)
	{
		Character.GetEquippedWeapon()->Attack(true);
	}

	if (CurrentAnimationTime >= Character.GetEquippedWeapon()->LightCombo[Index].EndApplyDamageTime)
	{
		Character.GetEquippedWeapon()->Attack(false);
	}
}

FHeroState* FHeroModeLightCombo::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (PlayerController->WasInputKeyJustPressed(EKeys::LeftMouseButton))
	{
		if (CurrentAnimationTime >= Character.GetEquippedWeapon()->LightCombo[Index].StartNewComboTime)
		{
			bShouldPerformNextAttack = true;
		}
	}

	if (CurrentAnimationTime >= AnimationDuration)
	{
		if (bShouldPerformNextAttack && Index < (Character.GetEquippedWeapon()->LightCombo.Num() - 1))
		{
			return new FHeroModeLightCombo(++Index);
		}

		if (Character.GetIsCharacterLockedOn())
		{
			return new FHeroModeLockedOn();
		}

		return new FHeroModeRunWithWeapon();
	}

	return nullptr;
}

void FHeroModeLightCombo::OnEnterState(ACyberHell_1Character& Character)
{
	CurrentAnimationTime = 0.f;
	bShouldPerformNextAttack = false;

	Character.EnableMovement(false);

	if (Character.GetMesh()->GetAnimInstance() && Character.GetEquippedWeapon()->LightCombo[Index].ComboMoveAnimation)
	{
		AnimationDuration = Character.GetMesh()->GetAnimInstance()->Montage_Play(
			Character.GetEquippedWeapon()->LightCombo[Index].ComboMoveAnimation);
	}

	Character.GetEquippedWeapon()->Attack(false);
	Character.GetEquippedWeapon()->SetCurrentComboMove(Character.GetEquippedWeapon()->LightCombo[Index]);
}

void FHeroModeLightCombo::OnExitState(ACyberHell_1Character& Character)
{
	Character.GetEquippedWeapon()->Attack(false);
	Character.EnableMovement(true);
}

void FHeroModeLightCombo::IsMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	bIsMontageEnded = true;
}

////////////////////////////////////////
///HEAVY_COMBO_STATE_SECTION///////////
//////////////////////////////////////

void FHeroModeHeavyCombo::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	CurrentAnimationTime += DeltaTime;

	if (CurrentAnimationTime >= Character.GetEquippedWeapon()->HeavyCombo[Index].StartApplyDamageTime &&
		CurrentAnimationTime <= Character.GetEquippedWeapon()->HeavyCombo[Index].EndApplyDamageTime)
	{
		Character.GetEquippedWeapon()->Attack(true);
	}

	if (CurrentAnimationTime >= Character.GetEquippedWeapon()->HeavyCombo[Index].EndApplyDamageTime)
	{
		Character.GetEquippedWeapon()->Attack(false);
	}
}

FHeroState* FHeroModeHeavyCombo::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (PlayerController->WasInputKeyJustPressed(EKeys::RightMouseButton))
	{
		if (CurrentAnimationTime >= Character.GetEquippedWeapon()->HeavyCombo[Index].StartNewComboTime)
		{
			if (CurrentAnimationTime <= AnimationDuration && Index < (Character.GetEquippedWeapon()->HeavyCombo.Num() - 1))
			{
				return new FHeroModeHeavyCombo(++Index);
			}
		}
	}

	if (CurrentAnimationTime >= AnimationDuration)
	{
		if (Character.GetIsCharacterLockedOn())
		{
			return new FHeroModeLockedOn();
		}

		return new FHeroModeRunWithWeapon();
	}

	return nullptr;
}

void FHeroModeHeavyCombo::OnEnterState(ACyberHell_1Character& Character)
{
	CurrentAnimationTime = 0.f;

	Character.EnableMovement(false);

	if (Character.GetMesh()->GetAnimInstance() && Character.GetEquippedWeapon()->HeavyCombo[Index].ComboMoveAnimation)
	{
		AnimationDuration = Character.GetMesh()->GetAnimInstance()->Montage_Play(
			Character.GetEquippedWeapon()->HeavyCombo[Index].ComboMoveAnimation);
	}

	Character.GetEquippedWeapon()->Attack(false);
	Character.GetEquippedWeapon()->SetCurrentComboMove(Character.GetEquippedWeapon()->HeavyCombo[Index]);
}

void FHeroModeHeavyCombo::OnExitState(ACyberHell_1Character& Character)
{
	Character.GetEquippedWeapon()->Attack(false);
	Character.EnableMovement(true);
}

////////////////////////////////////////
///LOCKED_ON_STATE_SECTION/////////////
//////////////////////////////////////

void FHeroModeLockedOn::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	if (Character.GetIsCharacterLockedOn())
	{
		Character.EnemyLockOn();
	}

	float DeltaX, DeltaY;
	Character.PlayerController->GetInputMouseDelta(DeltaX, DeltaY);

	if (DeltaX != 0 && Character.GetWorld()->GetTimeSeconds() >= LastTimeTargetChanged + ChangeTargetCooldawn)
	{
		ChangeTargetLockOn(Character, DeltaX);
		LastTimeTargetChanged = Character.GetWorld()->GetTimeSeconds();
	}
}

FHeroState* FHeroModeLockedOn::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (!Character.GetIsCharacterLockedOn() || PlayerController->WasInputKeyJustPressed(EKeys::F))
	{
		if (Character.GetCurrentLockedOnEnemy() != nullptr)
		{
			GameInstance->EventHandler->OnEnemyChangeTarget.Broadcast(Enemy->GetUniqueID());
		}
		Character.SetIsCharacterLockedOn(false);
		Character.SetCurrentLockedOnEnemy(nullptr);
		return new FHeroModeRunWithWeapon();
	}
	
	if (PlayerController->WasInputKeyJustPressed(EKeys::LeftMouseButton))
	{
		return new FHeroModeLightCombo(0);
	}

	if (PlayerController->WasInputKeyJustPressed(EKeys::RightMouseButton))
	{
		return new FHeroModeHeavyCombo(0);
	}

	return nullptr;
}

void FHeroModeLockedOn::OnEnterState(ACyberHell_1Character& Character)
{
	Character.EnableCameraRotationByPlayer(false);
	Character.bIsLerping = true;
	Enemy = Cast<AEnemyCharacter>(Character.GetCurrentLockedOnEnemy());
	GameInstance = Character.GetWorld()->GetGameInstance<UCyberHellGameInstance>();

	Enemy != nullptr ? Character.SetIsCharacterLockedOn(true) : Character.SetIsCharacterLockedOn(false);
	
	if (GameInstance->EventHandler && Enemy != nullptr)
	{
		GameInstance->EventHandler->OnEnemyLockOn.Broadcast(Enemy->GetUniqueID());
	}

	LastTimeTargetChanged = Character.GetWorld()->GetTimeSeconds();
}

void FHeroModeLockedOn::OnExitState(ACyberHell_1Character& Character)
{
	
}

////////////////////////////////////////
///JUMP_FROM_WALL_SECTION//////////////
//////////////////////////////////////

void FHeroModeJumpFromWall::Tick(ACyberHell_1Character& Character, float DeltaTime)
{
	
}

FHeroState* FHeroModeJumpFromWall::HandleInput(ACyberHell_1Character& Character, APlayerController* PlayerController)
{
	if (bIsMontageEnded)
	{
		return new FHeroModeRun();
	}

	return nullptr;
}

void FHeroModeJumpFromWall::OnEnterState(ACyberHell_1Character& Character)
{
	bIsMontageEnded = false;
	Character.EnableMovement(false);
	Character.SetHangingIdle(false);

	if (Character.GetMesh()->GetAnimInstance() && Character.GetClimbMontage())
	{
		Character.GetMesh()->GetAnimInstance()->Montage_Play(Character.GetJumpFromWallMontage(), 1.25f);
	}

	Delegate.BindRaw(this, &FHeroModeJumpFromWall::IsMontageEnded);
	Character.GetMesh()->GetAnimInstance()->Montage_SetBlendingOutDelegate(Delegate);
}

void FHeroModeJumpFromWall::OnExitState(ACyberHell_1Character& Character)
{
	Character.EnableMovement(true);
	JumpFromWall(Character);
}

void FHeroModeJumpFromWall::JumpFromWall(ACyberHell_1Character& Character)
{
	FVector VectorVelocity = Character.GetActorForwardVector() * (Character.ForwardJumpPower) + FVector(0, 0, Character.UpwardJumpPower);
	Character.LaunchCharacter(VectorVelocity, false, false);
	FRotator CharacterRotation = Character.GetActorRotation();
	Character.SetActorRotation(FRotator(CharacterRotation.Pitch, CharacterRotation.Yaw + Character.AngleOfJump, CharacterRotation.Roll));
}

void FHeroModeJumpFromWall::IsMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	bIsMontageEnded = true;
}
