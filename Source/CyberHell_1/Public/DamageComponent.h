// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DamageComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CYBERHELL_1_API UDamageComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDamageComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	UPROPERTY(EditAnywhere)
	class UAnimMontage* DamageAnimation;

	UPROPERTY(EditAnywhere)
	class USoundBase* HurtSound;

	UPROPERTY(EditAnywhere)
	class TSubclassOf<UCameraShake> CameraShakeComponent;

	UFUNCTION(BlueprintCallable)
	float GetMaxHealth() { return MaxHealth; }

	UFUNCTION(BlueprintCallable)
	float GetCurrentHealth() { return CurrentHealth; }

private:
	UFUNCTION()
	void OnDamageTaken(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, 
		class AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY()
	class UCyberHellGameInstance* GameInstance;

	UPROPERTY(EditAnywhere, Category = "HUD")
	float MaxHealth;

	UPROPERTY()
	float CurrentHealth;

	UPROPERTY(EditAnywhere, Category = "HUD")
	float MaxEnergy;

	UPROPERTY()
	float CurrentEnergy;

};
