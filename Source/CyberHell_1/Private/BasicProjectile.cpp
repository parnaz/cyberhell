// Fill out your copyright notice in the Description page of Project Settings.


#include "BasicProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "CyberHell_1Character.h"
#include "GameFramework/DamageType.h"

// Sets default values
ABasicProjectile::ABasicProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(FName("Collision Sphere"));
	RootComponent = CollisionSphere;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(FName("Mesh Component"));
	MeshComponent->SetupAttachment(RootComponent);

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(FName("Movement Component"));
	MovementComponent->InitialSpeed = 300.f;

	Damage = 20.f;
}

// Called when the game starts or when spawned
void ABasicProjectile::BeginPlay()
{
	Super::BeginPlay();

	PlayerCharacter = Cast<ACyberHell_1Character>(GetWorld()->GetFirstPlayerController()->GetCharacter());
	
	if (PlayerCharacter != nullptr)
	{
		MovementComponent->Velocity = (PlayerCharacter->GetActorLocation() - this->GetActorLocation()).GetSafeNormal() * 
			MovementComponent->InitialSpeed;
	}

	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ABasicProjectile::OnOverlapBegin);
	CollisionSphere->OnComponentHit.AddDynamic(this, &ABasicProjectile::OnHit);
}

void ABasicProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr)
	{
		OtherActor->OnTakeAnyDamage.Broadcast(OtherActor,
			Damage,
			Cast<UDamageType>(UDamageType::StaticClass()),
			GetWorld()->GetFirstPlayerController(),
			this);

		Destroy();
	}
}

void ABasicProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr)
	{
		Destroy();
	}
}

// Called every frame
void ABasicProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

