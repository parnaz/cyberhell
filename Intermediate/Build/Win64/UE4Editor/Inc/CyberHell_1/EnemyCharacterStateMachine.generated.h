// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CYBERHELL_1_EnemyCharacterStateMachine_generated_h
#error "EnemyCharacterStateMachine.generated.h already included, missing '#pragma once' in EnemyCharacterStateMachine.h"
#endif
#define CYBERHELL_1_EnemyCharacterStateMachine_generated_h

#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_RPC_WRAPPERS
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnemyCharacterStateMachine(); \
	friend struct Z_Construct_UClass_UEnemyCharacterStateMachine_Statics; \
public: \
	DECLARE_CLASS(UEnemyCharacterStateMachine, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCharacterStateMachine)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUEnemyCharacterStateMachine(); \
	friend struct Z_Construct_UClass_UEnemyCharacterStateMachine_Statics; \
public: \
	DECLARE_CLASS(UEnemyCharacterStateMachine, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCharacterStateMachine)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnemyCharacterStateMachine(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemyCharacterStateMachine) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCharacterStateMachine); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCharacterStateMachine); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCharacterStateMachine(UEnemyCharacterStateMachine&&); \
	NO_API UEnemyCharacterStateMachine(const UEnemyCharacterStateMachine&); \
public:


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCharacterStateMachine(UEnemyCharacterStateMachine&&); \
	NO_API UEnemyCharacterStateMachine(const UEnemyCharacterStateMachine&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCharacterStateMachine); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCharacterStateMachine); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEnemyCharacterStateMachine)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_PRIVATE_PROPERTY_OFFSET
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_13_PROLOG
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_RPC_WRAPPERS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_INCLASS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_INCLASS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CYBERHELL_1_API UClass* StaticClass<class UEnemyCharacterStateMachine>();

#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_RPC_WRAPPERS
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_RPC_WRAPPERS_NO_PURE_DECLS
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnemyCharacterNormalState(); \
	friend struct Z_Construct_UClass_UEnemyCharacterNormalState_Statics; \
public: \
	DECLARE_CLASS(UEnemyCharacterNormalState, UEnemyCharacterStateMachine, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCharacterNormalState)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUEnemyCharacterNormalState(); \
	friend struct Z_Construct_UClass_UEnemyCharacterNormalState_Statics; \
public: \
	DECLARE_CLASS(UEnemyCharacterNormalState, UEnemyCharacterStateMachine, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCharacterNormalState)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnemyCharacterNormalState(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemyCharacterNormalState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCharacterNormalState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCharacterNormalState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCharacterNormalState(UEnemyCharacterNormalState&&); \
	NO_API UEnemyCharacterNormalState(const UEnemyCharacterNormalState&); \
public:


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCharacterNormalState(UEnemyCharacterNormalState&&); \
	NO_API UEnemyCharacterNormalState(const UEnemyCharacterNormalState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCharacterNormalState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCharacterNormalState); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEnemyCharacterNormalState)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_PRIVATE_PROPERTY_OFFSET
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_33_PROLOG
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_RPC_WRAPPERS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_INCLASS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_INCLASS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_36_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CYBERHELL_1_API UClass* StaticClass<class UEnemyCharacterNormalState>();

#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_RPC_WRAPPERS
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_RPC_WRAPPERS_NO_PURE_DECLS
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnemyCharacterDrawingWeapon(); \
	friend struct Z_Construct_UClass_UEnemyCharacterDrawingWeapon_Statics; \
public: \
	DECLARE_CLASS(UEnemyCharacterDrawingWeapon, UEnemyCharacterStateMachine, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCharacterDrawingWeapon)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_INCLASS \
private: \
	static void StaticRegisterNativesUEnemyCharacterDrawingWeapon(); \
	friend struct Z_Construct_UClass_UEnemyCharacterDrawingWeapon_Statics; \
public: \
	DECLARE_CLASS(UEnemyCharacterDrawingWeapon, UEnemyCharacterStateMachine, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCharacterDrawingWeapon)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnemyCharacterDrawingWeapon(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemyCharacterDrawingWeapon) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCharacterDrawingWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCharacterDrawingWeapon); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCharacterDrawingWeapon(UEnemyCharacterDrawingWeapon&&); \
	NO_API UEnemyCharacterDrawingWeapon(const UEnemyCharacterDrawingWeapon&); \
public:


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCharacterDrawingWeapon(UEnemyCharacterDrawingWeapon&&); \
	NO_API UEnemyCharacterDrawingWeapon(const UEnemyCharacterDrawingWeapon&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCharacterDrawingWeapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCharacterDrawingWeapon); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEnemyCharacterDrawingWeapon)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_PRIVATE_PROPERTY_OFFSET
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_48_PROLOG
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_RPC_WRAPPERS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_INCLASS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_INCLASS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_51_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CYBERHELL_1_API UClass* StaticClass<class UEnemyCharacterDrawingWeapon>();

#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_RPC_WRAPPERS
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_RPC_WRAPPERS_NO_PURE_DECLS
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnemyCharacterChasePlayer(); \
	friend struct Z_Construct_UClass_UEnemyCharacterChasePlayer_Statics; \
public: \
	DECLARE_CLASS(UEnemyCharacterChasePlayer, UEnemyCharacterStateMachine, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCharacterChasePlayer)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_INCLASS \
private: \
	static void StaticRegisterNativesUEnemyCharacterChasePlayer(); \
	friend struct Z_Construct_UClass_UEnemyCharacterChasePlayer_Statics; \
public: \
	DECLARE_CLASS(UEnemyCharacterChasePlayer, UEnemyCharacterStateMachine, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCharacterChasePlayer)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnemyCharacterChasePlayer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemyCharacterChasePlayer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCharacterChasePlayer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCharacterChasePlayer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCharacterChasePlayer(UEnemyCharacterChasePlayer&&); \
	NO_API UEnemyCharacterChasePlayer(const UEnemyCharacterChasePlayer&); \
public:


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCharacterChasePlayer(UEnemyCharacterChasePlayer&&); \
	NO_API UEnemyCharacterChasePlayer(const UEnemyCharacterChasePlayer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCharacterChasePlayer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCharacterChasePlayer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEnemyCharacterChasePlayer)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_PRIVATE_PROPERTY_OFFSET
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_67_PROLOG
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_RPC_WRAPPERS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_INCLASS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_RPC_WRAPPERS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_INCLASS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_70_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CYBERHELL_1_API UClass* StaticClass<class UEnemyCharacterChasePlayer>();

#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_RPC_WRAPPERS
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_RPC_WRAPPERS_NO_PURE_DECLS
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEnemyCharacterBattleState(); \
	friend struct Z_Construct_UClass_UEnemyCharacterBattleState_Statics; \
public: \
	DECLARE_CLASS(UEnemyCharacterBattleState, UEnemyCharacterStateMachine, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCharacterBattleState)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_INCLASS \
private: \
	static void StaticRegisterNativesUEnemyCharacterBattleState(); \
	friend struct Z_Construct_UClass_UEnemyCharacterBattleState_Statics; \
public: \
	DECLARE_CLASS(UEnemyCharacterBattleState, UEnemyCharacterStateMachine, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UEnemyCharacterBattleState)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEnemyCharacterBattleState(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEnemyCharacterBattleState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCharacterBattleState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCharacterBattleState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCharacterBattleState(UEnemyCharacterBattleState&&); \
	NO_API UEnemyCharacterBattleState(const UEnemyCharacterBattleState&); \
public:


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEnemyCharacterBattleState(UEnemyCharacterBattleState&&); \
	NO_API UEnemyCharacterBattleState(const UEnemyCharacterBattleState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEnemyCharacterBattleState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEnemyCharacterBattleState); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEnemyCharacterBattleState)


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_PRIVATE_PROPERTY_OFFSET
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_85_PROLOG
#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_RPC_WRAPPERS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_INCLASS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_RPC_WRAPPERS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_INCLASS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h_88_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CYBERHELL_1_API UClass* StaticClass<class UEnemyCharacterBattleState>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CyberHell_rebuild_Source_CyberHell_1_Public_EnemyCharacterStateMachine_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
