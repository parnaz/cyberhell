// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CYBERHELL_1_CyberHellGameState_generated_h
#error "CyberHellGameState.generated.h already included, missing '#pragma once' in CyberHellGameState.h"
#endif
#define CYBERHELL_1_CyberHellGameState_generated_h

#define CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_RPC_WRAPPERS
#define CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACyberHellGameState(); \
	friend struct Z_Construct_UClass_ACyberHellGameState_Statics; \
public: \
	DECLARE_CLASS(ACyberHellGameState, AGameStateBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(ACyberHellGameState)


#define CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_INCLASS \
private: \
	static void StaticRegisterNativesACyberHellGameState(); \
	friend struct Z_Construct_UClass_ACyberHellGameState_Statics; \
public: \
	DECLARE_CLASS(ACyberHellGameState, AGameStateBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(ACyberHellGameState)


#define CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACyberHellGameState(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACyberHellGameState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACyberHellGameState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACyberHellGameState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACyberHellGameState(ACyberHellGameState&&); \
	NO_API ACyberHellGameState(const ACyberHellGameState&); \
public:


#define CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACyberHellGameState(ACyberHellGameState&&); \
	NO_API ACyberHellGameState(const ACyberHellGameState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACyberHellGameState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACyberHellGameState); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACyberHellGameState)


#define CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_PRIVATE_PROPERTY_OFFSET
#define CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_14_PROLOG
#define CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_RPC_WRAPPERS \
	CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_INCLASS \
	CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_INCLASS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CYBERHELL_1_API UClass* StaticClass<class ACyberHellGameState>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CyberHell_rebuild_Source_CyberHell_1_CyberHellGameState_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
