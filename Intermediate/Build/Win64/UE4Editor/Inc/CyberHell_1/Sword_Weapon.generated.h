// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CYBERHELL_1_Sword_Weapon_generated_h
#error "Sword_Weapon.generated.h already included, missing '#pragma once' in Sword_Weapon.h"
#endif
#define CYBERHELL_1_Sword_Weapon_generated_h

#define CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_RPC_WRAPPERS
#define CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASword_Weapon(); \
	friend struct Z_Construct_UClass_ASword_Weapon_Statics; \
public: \
	DECLARE_CLASS(ASword_Weapon, ABase_Weapon, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(ASword_Weapon)


#define CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASword_Weapon(); \
	friend struct Z_Construct_UClass_ASword_Weapon_Statics; \
public: \
	DECLARE_CLASS(ASword_Weapon, ABase_Weapon, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(ASword_Weapon)


#define CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASword_Weapon(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASword_Weapon) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASword_Weapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASword_Weapon); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASword_Weapon(ASword_Weapon&&); \
	NO_API ASword_Weapon(const ASword_Weapon&); \
public:


#define CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASword_Weapon(ASword_Weapon&&); \
	NO_API ASword_Weapon(const ASword_Weapon&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASword_Weapon); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASword_Weapon); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASword_Weapon)


#define CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_PRIVATE_PROPERTY_OFFSET
#define CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_12_PROLOG
#define CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_RPC_WRAPPERS \
	CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_INCLASS \
	CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_INCLASS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CYBERHELL_1_API UClass* StaticClass<class ASword_Weapon>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CyberHell_rebuild_Source_CyberHell_1_Sword_Weapon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
