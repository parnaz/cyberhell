// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CYBERHELL_1_MainGame_HUD_generated_h
#error "MainGame_HUD.generated.h already included, missing '#pragma once' in MainGame_HUD.h"
#endif
#define CYBERHELL_1_MainGame_HUD_generated_h

#define CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_RPC_WRAPPERS
#define CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMainGame_HUD(); \
	friend struct Z_Construct_UClass_AMainGame_HUD_Statics; \
public: \
	DECLARE_CLASS(AMainGame_HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(AMainGame_HUD)


#define CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAMainGame_HUD(); \
	friend struct Z_Construct_UClass_AMainGame_HUD_Statics; \
public: \
	DECLARE_CLASS(AMainGame_HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(AMainGame_HUD)


#define CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainGame_HUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainGame_HUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainGame_HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainGame_HUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainGame_HUD(AMainGame_HUD&&); \
	NO_API AMainGame_HUD(const AMainGame_HUD&); \
public:


#define CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainGame_HUD(AMainGame_HUD&&); \
	NO_API AMainGame_HUD(const AMainGame_HUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainGame_HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainGame_HUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMainGame_HUD)


#define CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_PRIVATE_PROPERTY_OFFSET
#define CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_14_PROLOG
#define CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_RPC_WRAPPERS \
	CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_INCLASS \
	CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_INCLASS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CYBERHELL_1_API UClass* StaticClass<class AMainGame_HUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CyberHell_rebuild_Source_CyberHell_1_MainGame_HUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
