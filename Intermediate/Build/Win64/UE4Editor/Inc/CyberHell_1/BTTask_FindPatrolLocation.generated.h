// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CYBERHELL_1_BTTask_FindPatrolLocation_generated_h
#error "BTTask_FindPatrolLocation.generated.h already included, missing '#pragma once' in BTTask_FindPatrolLocation.h"
#endif
#define CYBERHELL_1_BTTask_FindPatrolLocation_generated_h

#define CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_RPC_WRAPPERS
#define CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTTask_FindPatrolLocation(); \
	friend struct Z_Construct_UClass_UBTTask_FindPatrolLocation_Statics; \
public: \
	DECLARE_CLASS(UBTTask_FindPatrolLocation, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_FindPatrolLocation)


#define CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBTTask_FindPatrolLocation(); \
	friend struct Z_Construct_UClass_UBTTask_FindPatrolLocation_Statics; \
public: \
	DECLARE_CLASS(UBTTask_FindPatrolLocation, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CyberHell_1"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_FindPatrolLocation)


#define CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_FindPatrolLocation(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_FindPatrolLocation) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_FindPatrolLocation); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_FindPatrolLocation); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_FindPatrolLocation(UBTTask_FindPatrolLocation&&); \
	NO_API UBTTask_FindPatrolLocation(const UBTTask_FindPatrolLocation&); \
public:


#define CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_FindPatrolLocation(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_FindPatrolLocation(UBTTask_FindPatrolLocation&&); \
	NO_API UBTTask_FindPatrolLocation(const UBTTask_FindPatrolLocation&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_FindPatrolLocation); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_FindPatrolLocation); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_FindPatrolLocation)


#define CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_PRIVATE_PROPERTY_OFFSET
#define CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_12_PROLOG
#define CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_RPC_WRAPPERS \
	CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_INCLASS \
	CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_PRIVATE_PROPERTY_OFFSET \
	CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_INCLASS_NO_PURE_DECLS \
	CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CYBERHELL_1_API UClass* StaticClass<class UBTTask_FindPatrolLocation>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CyberHell_rebuild_Source_CyberHell_1_BTTask_FindPatrolLocation_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
