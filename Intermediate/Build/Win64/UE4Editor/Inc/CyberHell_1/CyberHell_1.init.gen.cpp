// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCyberHell_1_init() {}
	CYBERHELL_1_API UFunction* Z_Construct_UDelegateFunction_CyberHell_1_OnEnemyDeathSignature__DelegateSignature();
	CYBERHELL_1_API UFunction* Z_Construct_UDelegateFunction_CyberHell_1_OnEnemyLockOnSignature__DelegateSignature();
	CYBERHELL_1_API UFunction* Z_Construct_UDelegateFunction_CyberHell_1_OnPlayerDeathSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_CyberHell_1()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_CyberHell_1_OnEnemyDeathSignature__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_CyberHell_1_OnEnemyLockOnSignature__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_CyberHell_1_OnPlayerDeathSignature__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/CyberHell_1",
				SingletonFuncArray,
				ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x5664A866,
				0x606E089C,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
